# plotter

The plotter script can be used to make plots from trees. It contains the following files:

- plotTrees.py: main plotting script
- samples_trees.py: defines the list of samples
- cuts_1L2L.py: defines the regions
- variables.py: defines the variables
- subprocesses.py: can be used to run multiple plots in sequence

To make a plot of the MET variable (-v) in the preselection region (-r), run:
```
python3 plotTrees.py -v MET -r presel
```
with will have the cut right significance on the bottom panel.

To make the same plot but with the Data/MC on the bottom panel, the unblinding (-u) option shall be added. Run:
```
python3 plotTrees.py -v MET -r presel -u
```
It is also possible to run only subperiods with the -p option.
