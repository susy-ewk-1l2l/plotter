"""
Specify cut strings for 2L0J region definitions - copied from Rupert's

Write output with
python -c "import cuts;cuts.write()"
"""

trigger_matching = '((trigMatch_singleElectronTrig || trigMatch_singleMuonTrig) || trigMatch_metTrig)'

def join(cuts):
    """Join boolean logic list into an executable string"""
    return cuts# hack "(" + " && ".join(cuts) + ")"


def join_dict(d):
    """Join all values in dict d"""
    return {key: join(value) for key, value in d.items()}


def replace(L, was, now):
    """Return a copy of L with the first `was' replaced by `now'"""
    newL = list(L)
    newL[newL.index(was)] = now
    return newL


def configure(precut=False):
    """Return dict of cuts. precut True assumes dev skim slim"""
    # cuts for now
    cuts = join_dict({

        'presel'      : ['1.', trigger_matching],
         
        '1L'          : [trigger_matching, 'nLep_base==1', 'nLep_signal==1'],
        '2L'          : [trigger_matching, 'nLep_base==2', 'nLep_signal==2'],

        '1L-el'       : [trigger_matching, 'nLep_base==1', 'nLep_signal==1', 'AnalysisType==1'],
        '1L-mu'       : [trigger_matching, 'nLep_base==1', 'nLep_signal==1', 'AnalysisType==2'],

        '2L-ee'       : [trigger_matching, 'nLep_base==2', 'nLep_signal==2', 'AnalysisType==1', 'IsSF==1'],
        '2L-mumu'     : [trigger_matching, 'nLep_base==2', 'nLep_signal==2', 'AnalysisType==2', 'IsSF==1'],
        '2L-emu_mue'  : [trigger_matching, 'nLep_base==2', 'nLep_signal==2', 'AnalysisType==1', 'IsSF==0'],


        '1LR' : [trigger_matching, 'nLep_base==1', 'nLep_signal==1', 'dPhi_lep_met < 2.6', 'lep1Pt > 25.', 'nFatjets==0', 'mjj>70.', 'mjj<110.'],
        '1LB' : [trigger_matching, 'nLep_base==1', 'nLep_signal==1', 'dPhi_lep_met < 2.6', 'lep1Pt > 25.', 'nFatjets==1'],


        '2LR': [trigger_matching, 'nLep_base==2', 'nLep_signal==2', 'IsSF==1', 'IsOS==1', 'dPhi_lep_met < 2.6', 'mll>=71', 'mll<=111', 'nJet30>=2', 'nBJet30_DL1<=2', 'nFatjets==0'],
        '2LB': [trigger_matching, 'nLep_base==2', 'nLep_signal==2', 'IsSF==1', 'IsOS==1', 'dPhi_lep_met < 2.6', 'mll>=71', 'mll<=111', 'nBJet30_DL1<=2', 'nFatjets==1'],

    })
    return cuts


def write(outname="regions.h"):
    """Write out a header file with macros for these regions"""
    cuts = configure(precut=False)
    with open(outname, "w") as f:
        for R in cuts:
            # switch to vector pointer syntax
            S = cuts[R].replace("[", "->at(").replace("]", ")")
            f.write("#define _" + R + "_ " + S + "\n")

d_cuts = configure(True)
def configure_cuts(var, add_cuts, sig_reg, isData18, allNCuts, print_cuts=True):
    # From cut lists, build cut string doing N-1 appropriately
    l_cuts = d_cuts[sig_reg]

    # In case we do NOT want to skip cut on variable to be plotted
    #if allNCuts or var in l_blinding_cuts or 'presel' in sig_reg or 'VR-com' in sig_reg:
    if allNCuts:
        l_cuts_nMinus1 = l_cuts
    else:
        # (N-1) if variable to be plotted is in cut, do not cut on it 
        l_cuts_nMinus1 = [cut for cut in l_cuts if var not in cut]

    # join cuts with && (AND) operator
    cuts = ' && '.join(l_cuts_nMinus1)
    added_cuts = cuts + ' && ' + add_cuts

    if print_cuts:
        print('===============================================')
        print('Cuts applied:')
        for x in l_cuts_nMinus1:
          print (x)
        print('-----------------------------------------------')
        print( 'Unweighted final cut-string:', added_cuts)
        print('===============================================')

    return added_cuts
