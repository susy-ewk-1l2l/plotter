
#____________________________________________________________________________
def configure_samples(period):
  from ROOT import TColor
  from ROOT import kBlack,kWhite,kGray,kRed,kPink,kMagenta,kViolet,kBlue,kAzure,kCyan,kTeal,kGreen,kSpring,kYellow,kOrange

  # Blues
  myLighterBlue=kAzure-2
  myLightBlue  =TColor.GetColor('#9ecae1')
  myMediumBlue =TColor.GetColor('#0868ac')
  myDarkBlue   =TColor.GetColor('#08306b')

  # Greens
  myLightGreen   =TColor.GetColor('#c7e9c0')
  myMediumGreen  =TColor.GetColor('#41ab5d')
  myDarkGreen    =TColor.GetColor('#006d2c')

  # Oranges
  myLighterOrange=TColor.GetColor('#ffeda0')
  myLightOrange  =TColor.GetColor('#fec49f')
  myMediumOrange =TColor.GetColor('#fe9929')

  # Greys
  myLightestGrey=TColor.GetColor('#f0f0f0')
  myLighterGrey=TColor.GetColor('#e3e3e3')
  myLightGrey  =TColor.GetColor('#969696')

  # Pinks
  myLightPink = TColor.GetColor('#fde0dd')
  myMediumPink = TColor.GetColor('#fcc5c0')
  myDarkPink = TColor.GetColor('#dd3497')

  # Purples
  myLightPurple   = TColor.GetColor('#967BB6')
  myMediumPurple  = TColor.GetColor('#9370db')
  myDarkPurple    = TColor.GetColor('#522888')

  # Turquoise
  myLightTurquoise  = TColor.GetColor('#7FB1B2')
  myMediumTurquoise = TColor.GetColor('#539798')
  myDarkTurquoise   = TColor.GetColor('#297D7D')

  # Reds
  myMediumRed = TColor.GetColor('#EE220D')
  myLightRed = TColor.GetColor('#EE220D')
  myDarkRed = TColor.GetColor('#751e15')
  
  d_samp = {
    'data'     :{'type':'data', 'leg':'data',    'f_color':kBlack,           'l_color':0,  'path' :'data_merged_processed.root'},
    'wjets'    :{'type':'bkg',  'leg':'Wjets',   'f_color':myLighterBlue,    'l_color':0,  'path' :'wjets_merged_processed.root'},
    'ttbar'    :{'type':'bkg',  'leg':'ttbar',   'f_color':myLightGreen,     'l_color':0,  'path' :'ttbar_merged_processed.root'},
    'diboson'  :{'type':'bkg',  'leg':'diboson', 'f_color':myMediumOrange,   'l_color':0,  'path' :'diboson_merged_processed.root'},
    'singletop':{'type':'bkg',  'leg':'Wt',      'f_color':myMediumGreen,    'l_color':0,  'path' :'singletop_merged_processed.root'},
    'zjets'    :{'type':'bkg',  'leg':'Zjets',   'f_color':myDarkTurquoise,  'l_color':0,  'path' :'zjets_merged_processed.root'},
    'Vgamma'   :{'type':'bkg',  'leg':'Vgamma',  'f_color':myMediumRed,      'l_color':0,  'path' :'Vgamma_merged_processed.root'},
    'gammajet' :{'type':'bkg',  'leg':'gammajet','f_color':myLightTurquoise, 'l_color':0,  'path' :'gammajet_merged_processed.root'},
    'higgs'    :{'type':'bkg',  'leg':'higgs',   'f_color':myMediumTurquoise,'l_color':0,  'path' :'higgs_merged_processed.root'},
    'dijet'    :{'type':'bkg',  'leg':'dijet',   'f_color':kOrange,          'l_color':0,  'path' :'dijet_merged_processed.root'},
    'triboson' :{'type':'bkg',  'leg':'triboson','f_color':myLightGrey,      'l_color':0,  'path' :'triboson_merged_processed.root'},
        
    # Signals
    'signal_200_50' :{'type':'sig','leg':'(200, 50)','l_color':  myDarkPurple,'path':'signal_200_50_merged_processed.root'},
    'signal_800_0' :{'type':'sig','leg':'(800, 0)','l_color':  myMediumPurple,'path':'signal_800_0_merged_processed.root'},
    }

  return d_samp


