import subprocess

def execute_commands(commands):
    for command in commands:
        process = subprocess.Popen(command, shell=True)
        process.wait()  # Wait for the current command to finish

if __name__ == "__main__":
    commands = [
        "python3 plotTrees.py -v nJet30        -r presel -u -p 2018",
        "python3 plotTrees.py -v nLep_signal   -r presel -u -p 2018",
        "python3 plotTrees.py -v lep1Pt        -r presel -u -p 2018",
        "python3 plotTrees.py -v lep1Eta       -r presel -u -p 2018",
        "python3 plotTrees.py -v lep1Phi       -r presel -u -p 2018",
        "python3 plotTrees.py -v met           -r presel -u -p 2018",
        "python3 plotTrees.py -v met_Signif    -r presel -u -p 2018",
        "python3 plotTrees.py -v met_Phi       -r presel -u -p 2018",
        "python3 plotTrees.py -v dPhi_lep_met  -r presel -u -p 2018",
        "python3 plotTrees.py -v mt            -r presel -u -p 2018",
        "python3 plotTrees.py -v jet1Pt        -r presel -u -p 2018",
        "python3 plotTrees.py -v jet1Eta       -r presel -u -p 2018",
        "python3 plotTrees.py -v jet1Phi       -r presel -u -p 2018",
        "python3 plotTrees.py -v nFatjets      -r presel -u -p 2018",
        "python3 plotTrees.py -v fatjet1Pt     -r presel -u -p 2018",
        "python3 plotTrees.py -v fatjet1Eta    -r presel -u -p 2018",
        "python3 plotTrees.py -v fatjet1Phi    -r presel -u -p 2018",
        "python3 plotTrees.py -v fatjet1M      -r presel -u -p 2018",
        "python3 plotTrees.py -v fatjet1Tau21  -r presel -u -p 2018",
        "python3 plotTrees.py -v fatjet1Tau32  -r presel -u -p 2018",
        "python3 plotTrees.py -v fatjet1D2     -r presel -u -p 2018",
        "python3 plotTrees.py -v mt2           -r presel -u -p 2018",
        "python3 plotTrees.py -v nBJet30       -r presel -u -p 2018",
        "python3 plotTrees.py -v bjet1Pt       -r presel -u -p 2018",
        "python3 plotTrees.py -v bjet1Eta      -r presel -u -p 2018",
        "python3 plotTrees.py -v bjet1Phi      -r presel -u -p 2018",
        "python3 plotTrees.py -v amt2          -r presel -u -p 2018",
        "python3 plotTrees.py -v mct           -r presel -u -p 2018",
        "python3 plotTrees.py -v mct2          -r presel -u -p 2018",
        "python3 plotTrees.py -v mll           -r presel -u -p 2018",
        "python3 plotTrees.py -v Rll           -r presel -u -p 2018",
        "python3 plotTrees.py -v Ptll          -r presel -u -p 2018",
        "python3 plotTrees.py -v dPhiPllMet    -r presel -u -p 2018",
        "python3 plotTrees.py -v dPhill        -r presel -u -p 2018",
        "python3 plotTrees.py -v mjj           -r presel -u -p 2018",
        "python3 plotTrees.py -v Rjj           -r presel -u -p 2018",
    ]
    # Preselection
    '''
    commands = [
        "python3 plotTrees.py -v lep1Pt       -r presel -u -p 2015-18",
        "python3 plotTrees.py -v met          -r presel -u -p 2015-18",
        "python3 plotTrees.py -v nJet30       -r presel -u -p 2015-18",
        "python3 plotTrees.py -v dPhi_lep_met -r presel -u -p 2015-18",
        "python3 plotTrees.py -v mt           -r presel -u -p 2015-18",
        "python3 plotTrees.py -v jet1Pt       -r presel -u -p 2015-18",
        "python3 plotTrees.py -v nLep_signal  -r presel -u -p 2015-18",
    ]
    '''
    '''   
    commands = [
        "python3 plotTrees.py -v lep1Pt       -r presel -u -p 2022-23",
        "python3 plotTrees.py -v met          -r presel -u -p 2022-23",
        "python3 plotTrees.py -v nJet30       -r presel -u -p 2022-23",
        "python3 plotTrees.py -v dPhi_lep_met -r presel -u -p 2022-23",
        "python3 plotTrees.py -v mt           -r presel -u -p 2022-23",
        "python3 plotTrees.py -v jet1Pt       -r presel -u -p 2022-23",
        "python3 plotTrees.py -v nLep_signal  -r presel -u -p 2022-23",
    ]
    '''      
    # 1L
    '''
    commands = [
        "python3 plotTrees.py -v lep1Pt       -r 1L -u -p 2015-18",
        "python3 plotTrees.py -v met          -r 1L -u -p 2015-18",
        "python3 plotTrees.py -v nJet30       -r 1L -u -p 2015-18",
        "python3 plotTrees.py -v dPhi_lep_met -r 1L -u -p 2015-18",
        "python3 plotTrees.py -v mt           -r 1L -u -p 2015-18",
        "python3 plotTrees.py -v jet1Pt       -r 1L -u -p 2015-18",
    ]
    '''
    '''
    commands = [
        "python3 plotTrees.py -v lep1Pt       -r 1L -u -p 2022-23",
        "python3 plotTrees.py -v met          -r 1L -u -p 2022-23",
        "python3 plotTrees.py -v nJet30       -r 1L -u -p 2022-23",
        "python3 plotTrees.py -v dPhi_lep_met -r 1L -u -p 2022-23",
        "python3 plotTrees.py -v mt           -r 1L -u -p 2022-23",
        "python3 plotTrees.py -v jet1Pt       -r 1L -u -p 2022-23",
    ]
    '''
    # 1L-electron
    '''
    commands = [
        "python3 plotTrees.py -v lep1Pt       -r 1L-electron -u -p 2015-18",
        "python3 plotTrees.py -v met          -r 1L-electron -u -p 2015-18",
        "python3 plotTrees.py -v nJet30       -r 1L-electron -u -p 2015-18",
        "python3 plotTrees.py -v dPhi_lep_met -r 1L-electron -u -p 2015-18",
        "python3 plotTrees.py -v mt           -r 1L-electron -u -p 2015-18",
        "python3 plotTrees.py -v jet1Pt       -r 1L-electron -u -p 2015-18",
    ]
    '''
    '''
    commands = [
        "python3 plotTrees.py -v lep1Pt       -r 1L-electron -u -p 2022-23",
        "python3 plotTrees.py -v met          -r 1L-electron -u -p 2022-23",
        "python3 plotTrees.py -v nJet30       -r 1L-electron -u -p 2022-23",
        "python3 plotTrees.py -v dPhi_lep_met -r 1L-electron -u -p 2022-23",
        "python3 plotTrees.py -v mt           -r 1L-electron -u -p 2022-23",
        "python3 plotTrees.py -v jet1Pt       -r 1L-electron -u -p 2022-23",
    ]
    '''
    # 1L-muon
    '''
    commands = [
        "python3 plotTrees.py -v lep1Pt       -r 1L-muon -u -p 2015-18",
        "python3 plotTrees.py -v met          -r 1L-muon -u -p 2015-18",
        "python3 plotTrees.py -v nJet30       -r 1L-muon -u -p 2015-18",
        "python3 plotTrees.py -v dPhi_lep_met -r 1L-muon -u -p 2015-18",
        "python3 plotTrees.py -v mt           -r 1L-muon -u -p 2015-18",
        "python3 plotTrees.py -v jet1Pt       -r 1L-muon -u -p 2015-18",
    ]
    '''
    '''
    commands = [
        "python3 plotTrees.py -v lep1Pt       -r 1L-muon -u -p 2022-23",
        "python3 plotTrees.py -v met          -r 1L-muon -u -p 2022-23",
        "python3 plotTrees.py -v nJet30       -r 1L-muon -u -p 2022-23",
        "python3 plotTrees.py -v dPhi_lep_met -r 1L-muon -u -p 2022-23",
        "python3 plotTrees.py -v mt           -r 1L-muon -u -p 2022-23",
        "python3 plotTrees.py -v jet1Pt       -r 1L-muon -u -p 2022-23",
    ]
    '''
    # 2L
    '''
    commands = [
        "python3 plotTrees.py -v lep1Pt       -r 2L -u -p 2015-18",
        "python3 plotTrees.py -v lep2Pt       -r 2L -u -p 2015-18",
        "python3 plotTrees.py -v met          -r 2L -u -p 2015-18",
        "python3 plotTrees.py -v nJet30       -r 2L -u -p 2015-18",
        "python3 plotTrees.py -v dPhi_lep_met -r 2L -u -p 2015-18",
        "python3 plotTrees.py -v mt           -r 2L -u -p 2015-18",
        "python3 plotTrees.py -v mll          -r 2L -u -p 2015-18",
        "python3 plotTrees.py -v jet1Pt       -r 2L -u -p 2015-18",
    ]
    '''
    '''
    commands = [
        "python3 plotTrees.py -v lep1Pt       -r 2L -u -p 2022-23",
        "python3 plotTrees.py -v lep2Pt       -r 2L -u -p 2022-23",
        "python3 plotTrees.py -v met          -r 2L -u -p 2022-23",
        "python3 plotTrees.py -v nJet30       -r 2L -u -p 2022-23",
        "python3 plotTrees.py -v dPhi_lep_met -r 2L -u -p 2022-23",
        "python3 plotTrees.py -v mt           -r 2L -u -p 2022-23",
        "python3 plotTrees.py -v mll          -r 2L -u -p 2022-23",
        "python3 plotTrees.py -v jet1Pt       -r 2L -u -p 2022-23",
    ]
    '''
    # 2L-ee
    '''
    commands = [
        "python3 plotTrees.py -v lep1Pt       -r 2L-ee -u -p 2015-18",
        "python3 plotTrees.py -v lep2Pt       -r 2L-ee -u -p 2015-18",
        "python3 plotTrees.py -v met          -r 2L-ee -u -p 2015-18",
        "python3 plotTrees.py -v nJet30       -r 2L-ee -u -p 2015-18",
        "python3 plotTrees.py -v dPhi_lep_met -r 2L-ee -u -p 2015-18",
        "python3 plotTrees.py -v mt           -r 2L-ee -u -p 2015-18",
        "python3 plotTrees.py -v mll          -r 2L-ee -u -p 2015-18",
        "python3 plotTrees.py -v jet1Pt       -r 2L-ee -u -p 2015-18",
    ]
    '''
    '''
    commands = [
        "python3 plotTrees.py -v lep1Pt       -r 2L-ee -u -p 2022-23",
        "python3 plotTrees.py -v lep2Pt       -r 2L-ee -u -p 2022-23",
        "python3 plotTrees.py -v met          -r 2L-ee -u -p 2022-23",
        "python3 plotTrees.py -v nJet30       -r 2L-ee -u -p 2022-23",
        "python3 plotTrees.py -v dPhi_lep_met -r 2L-ee -u -p 2022-23",
        "python3 plotTrees.py -v mt           -r 2L-ee -u -p 2022-23",
        "python3 plotTrees.py -v mll          -r 2L-ee -u -p 2022-23",
        "python3 plotTrees.py -v jet1Pt       -r 2L-ee -u -p 2022-23",
    ]
    '''  
    # 2L-mumu
    '''
    commands = [
        "python3 plotTrees.py -v lep1Pt       -r 2L-mumu -u -p 2015-18",
        "python3 plotTrees.py -v lep2Pt       -r 2L-mumu -u -p 2015-18",
        "python3 plotTrees.py -v met          -r 2L-mumu -u -p 2015-18",
        "python3 plotTrees.py -v nJet30       -r 2L-mumu -u -p 2015-18",
        "python3 plotTrees.py -v dPhi_lep_met -r 2L-mumu -u -p 2015-18",
        "python3 plotTrees.py -v mt           -r 2L-mumu -u -p 2015-18",
        "python3 plotTrees.py -v mll          -r 2L-mumu -u -p 2015-18",
        "python3 plotTrees.py -v jet1Pt       -r 2L-mumu -u -p 2015-18",
    ]
    '''
    '''
    commands = [
        "python3 plotTrees.py -v lep1Pt       -r 2L-mumu -u -p 2022-23",
        "python3 plotTrees.py -v lep2Pt       -r 2L-mumu -u -p 2022-23",
        "python3 plotTrees.py -v met          -r 2L-mumu -u -p 2022-23",
        "python3 plotTrees.py -v nJet30       -r 2L-mumu -u -p 2022-23",
        "python3 plotTrees.py -v dPhi_lep_met -r 2L-mumu -u -p 2022-23",
        "python3 plotTrees.py -v mt           -r 2L-mumu -u -p 2022-23",
        "python3 plotTrees.py -v mll          -r 2L-mumu -u -p 2022-23",
        "python3 plotTrees.py -v jet1Pt       -r 2L-mumu -u -p 2022-23",
    ]
    '''      
    # 2L-emu/mue
    '''
    commands = [
        "python3 plotTrees.py -v lep1Pt       -r 2L-emu_mue -u -p 2015-18",
        "python3 plotTrees.py -v lep2Pt       -r 2L-emu_mue -u -p 2015-18",
        "python3 plotTrees.py -v met          -r 2L-emu_mue -u -p 2015-18",
        "python3 plotTrees.py -v nJet30       -r 2L-emu_mue -u -p 2015-18",
        "python3 plotTrees.py -v dPhi_lep_met -r 2L-emu_mue -u -p 2015-18",
        "python3 plotTrees.py -v mt           -r 2L-emu_mue -u -p 2015-18",
        "python3 plotTrees.py -v mll          -r 2L-emu_mue -u -p 2015-18",
        "python3 plotTrees.py -v jet1Pt       -r 2L-emu_mue -u -p 2015-18",
    ]
    '''
    '''
    commands = [
        "python3 plotTrees.py -v lep1Pt       -r 2L-emu_mue -u -p 2022-23",
        "python3 plotTrees.py -v lep2Pt       -r 2L-emu_mue -u -p 2022-23",
        "python3 plotTrees.py -v met          -r 2L-emu_mue -u -p 2022-23",
        "python3 plotTrees.py -v nJet30       -r 2L-emu_mue -u -p 2022-23",
        "python3 plotTrees.py -v dPhi_lep_met -r 2L-emu_mue -u -p 2022-23",
        "python3 plotTrees.py -v mt           -r 2L-emu_mue -u -p 2022-23",
        "python3 plotTrees.py -v mll          -r 2L-emu_mue -u -p 2022-23",
        "python3 plotTrees.py -v jet1Pt       -r 2L-emu_mue -u -p 2022-23",
    ]
    '''


    # 1L resolved
    '''
    commands = [
        "python3 plotTrees.py -v lep1Pt       -r Selection_1LR -u -p 2015-18",
        "python3 plotTrees.py -v met          -r Selection_1LR -u -p 2015-18",
        "python3 plotTrees.py -v nJet30       -r Selection_1LR -u -p 2015-18",
        "python3 plotTrees.py -v dPhi_lep_met -r Selection_1LR -u -p 2015-18",
        "python3 plotTrees.py -v mt           -r Selection_1LR -u -p 2015-18",
        "python3 plotTrees.py -v mjj          -r Selection_1LR -u -p 2015-18",
        "python3 plotTrees.py -v nBJet30_DL1  -r Selection_1LR -u -p 2015-18",
        "python3 plotTrees.py -v jet1Pt       -r Selection_1LR -u -p 2015-18",
        "python3 plotTrees.py -v nFatjets     -r Selection_1LR -u -p 2015-18",
    ]
    '''

    '''
    commands = [
        "python3 plotTrees.py -v lep1Pt       -r Selection_1LR -u -p 2022-23",
        "python3 plotTrees.py -v met          -r Selection_1LR -u -p 2022-23",
        "python3 plotTrees.py -v nJet30       -r Selection_1LR -u -p 2022-23",
        "python3 plotTrees.py -v dPhi_lep_met -r Selection_1LR -u -p 2022-23",
        "python3 plotTrees.py -v mt           -r Selection_1LR -u -p 2022-23",
        "python3 plotTrees.py -v mjj          -r Selection_1LR -u -p 2022-23",
        "python3 plotTrees.py -v nBJet30_DL1  -r Selection_1LR -u -p 2022-23",
        "python3 plotTrees.py -v jet1Pt       -r Selection_1LR -u -p 2022-23",
        "python3 plotTrees.py -v nFatjets     -r Selection_1LR -u -p 2022-23",
    ]
    '''

    # 2L resolved
    '''
    commands = [
        "python3 plotTrees.py -v lep1Pt       -r Selection_2LR -u -p 2015-18",
        "python3 plotTrees.py -v lep2Pt       -r Selection_2LR -u -p 2015-18",
        "python3 plotTrees.py -v met          -r Selection_2LR -u -p 2015-18",
        "python3 plotTrees.py -v nJet30       -r Selection_2LR -u -p 2015-18",
        "python3 plotTrees.py -v dPhi_lep_met -r Selection_2LR -u -p 2015-18",
        "python3 plotTrees.py -v mt           -r Selection_2LR -u -p 2015-18",
        "python3 plotTrees.py -v mll          -r Selection_2LR -u -p 2015-18",
        "python3 plotTrees.py -v jet1Pt       -r Selection_2LR -u -p 2015-18",
        "python3 plotTrees.py -v nFatjets     -r Selection_2LR -u -p 2015-18",
    ]
    '''
    
    '''
    commands = [
        "python3 plotTrees.py -v lep1Pt       -r Selection_2LR -u -p 2022-23",
        "python3 plotTrees.py -v lep2Pt       -r Selection_2LR -u -p 2022-23",
        "python3 plotTrees.py -v met          -r Selection_2LR -u -p 2022-23",
        "python3 plotTrees.py -v nJet30       -r Selection_2LR -u -p 2022-23",
        "python3 plotTrees.py -v dPhi_lep_met -r Selection_2LR -u -p 2022-23",
        "python3 plotTrees.py -v mt           -r Selection_2LR -u -p 2022-23",
        "python3 plotTrees.py -v mll          -r Selection_2LR -u -p 2022-23",
        "python3 plotTrees.py -v jet1Pt       -r Selection_2LR -u -p 2022-23",
        "python3 plotTrees.py -v nFatjets     -r Selection_2LR -u -p 2022-23",
    ]
    '''




    # 1L boosted (only Run 2, missing fat jets in Run 3 production)
    '''
    commands = [
        "python3 plotTrees.py -v lep1Pt       -r Selection_1LB -u -p 2015-18",
        "python3 plotTrees.py -v met          -r Selection_1LB -u -p 2015-18",
        "python3 plotTrees.py -v nJet30       -r Selection_1LB -u -p 2015-18",
        "python3 plotTrees.py -v dPhi_lep_met -r Selection_1LB -u -p 2015-18",
        "python3 plotTrees.py -v mt           -r Selection_1LB -u -p 2015-18",
        "python3 plotTrees.py -v mjj          -r Selection_1LB -u -p 2015-18",
        "python3 plotTrees.py -v nBJet30_DL1  -r Selection_1LB -u -p 2015-18",
        "python3 plotTrees.py -v jet1Pt       -r Selection_1LB -u -p 2015-18",
 
        "python3 plotTrees.py -v fatjet1Pt       -r Selection_1LB -u -p 2015-18",
        "python3 plotTrees.py -v fatjet1Eta      -r Selection_1LB -u -p 2015-18",
        "python3 plotTrees.py -v fatjet1Phi      -r Selection_1LB -u -p 2015-18",
        "python3 plotTrees.py -v fatjet1M        -r Selection_1LB -u -p 2015-18",
        "python3 plotTrees.py -v fatjet1Wtagged  -r Selection_1LB -u -p 2015-18",
        "python3 plotTrees.py -v fatjet1Ztagged  -r Selection_1LB -u -p 2015-18",
        "python3 plotTrees.py -v fatjet1D2       -r Selection_1LB -u -p 2015-18",
        "python3 plotTrees.py -v fatjet1Tau32    -r Selection_1LB -u -p 2015-18",
        "python3 plotTrees.py -v fatjet1nTrk     -r Selection_1LB -u -p 2015-18",
        #"python3 plotTrees.py -v fatjet2Pt       -r Selection_1LB -u -p 2015-18",
        #"python3 plotTrees.py -v fatjet2M        -r Selection_1LB -u -p 2015-18",
    ]
    '''   


    # 2L boosted (only Run 2, missing fat jets in Run 3 production)
    '''
    commands = [
        "python3 plotTrees.py -v lep1Pt       -r Selection_2LB -u -p 2015-18",
        "python3 plotTrees.py -v met          -r Selection_2LB -u -p 2015-18",
        "python3 plotTrees.py -v nJet30       -r Selection_2LB -u -p 2015-18",
        "python3 plotTrees.py -v dPhi_lep_met -r Selection_2LB -u -p 2015-18",
        "python3 plotTrees.py -v mt           -r Selection_2LB -u -p 2015-18",
        "python3 plotTrees.py -v mjj          -r Selection_2LB -u -p 2015-18",
        "python3 plotTrees.py -v nBJet30_DL1  -r Selection_2LB -u -p 2015-18",
        "python3 plotTrees.py -v jet1Pt       -r Selection_2LB -u -p 2015-18",
 
        "python3 plotTrees.py -v fatjet1Pt       -r Selection_2LB -u -p 2015-18",
        "python3 plotTrees.py -v fatjet1Eta      -r Selection_2LB -u -p 2015-18",
        "python3 plotTrees.py -v fatjet1Phi      -r Selection_2LB -u -p 2015-18",
        "python3 plotTrees.py -v fatjet1M        -r Selection_2LB -u -p 2015-18",
        "python3 plotTrees.py -v fatjet1Wtagged  -r Selection_2LB -u -p 2015-18",
        "python3 plotTrees.py -v fatjet1Ztagged  -r Selection_2LB -u -p 2015-18",
        "python3 plotTrees.py -v fatjet1D2       -r Selection_2LB -u -p 2015-18",
        "python3 plotTrees.py -v fatjet1Tau32    -r Selection_2LB -u -p 2015-18",
        "python3 plotTrees.py -v fatjet1nTrk     -r Selection_2LB -u -p 2015-18",
        #"python3 plotTrees.py -v fatjet2Pt       -r Selection_2LB -u -p 2015-18",
        #"python3 plotTrees.py -v fatjet2M        -r Selection_2LB -u -p 2015-18",
    ] 
    '''


    execute_commands(commands)
