def configure_vars(sig_reg):

  # format for each variable is
  #'var_name_in_ntuple':{TLaTeX axis entry, units, Nbins, xmin, xmax, arrow position (for N-1 plots), arrow direction}
  # to do variable bin widths, place 'var' as value of 'hXNbins' and specify lower bin edges as 'binsLowE':[0,4,5,11,15,20,40,60]
  # e.g.     'lep2Pt':{'tlatex':'p_{T}(#font[12]{l}_{2})','units':'GeV','hXNbins':'var','hXmin':0,'hXmax':60,'binsLowE':[0,4,5,11,15,20,40,60],'cut_pos':200,'cut_dir':'left'}, 

  d_vars = {

    #---------------------------------
    #my saved variables
    'met'                 :{'tlatex':'E_{T}^{miss}',                        'units':'GeV',  'hXNbins':20,    'hXmin':200.,   'hXmax':1000.,   'cut_pos':230,  'cut_dir':'right'},
    'met_Signif'       :{'tlatex':'E_{T}^{miss} significance',           'units':'',  'hXNbins':30,    'hXmin':0.,   'hXmax':30.,   'cut_pos':230,  'cut_dir':'right'},
    'met_Phi'                 :{'tlatex':'#phi(E_{T}^{miss})',                'units':'',  'hXNbins':70,    'hXmin':-3.5,   'hXmax':3.5,   'cut_pos':230,  'cut_dir':'right'},
    'mt'                  :{'tlatex':'m_{T}',                               'units':'GeV',  'hXNbins':20,    'hXmin':0.,     'hXmax':2000.,   'cut_pos':230,  'cut_dir':'right'},    
    'mt2'                  :{'tlatex':'m_{T2}',                               'units':'GeV',  'hXNbins':20,    'hXmin':0.,     'hXmax':2000.,   'cut_pos':230,  'cut_dir':'right'},
    'nJet30'              :{'tlatex':'Number of jets',                      'units':'',     'hXNbins':5,     'hXmin':0.,     'hXmax':5.,      'cut_pos':230,  'cut_dir':'right'},
    'nBJet30'         :{'tlatex':'Number of b-jets',                    'units':'',     'hXNbins':5,     'hXmin':0.,     'hXmax':5.,      'cut_pos':230,  'cut_dir':'right'},
    'nLep_signal'         :{'tlatex':'Signal leptons',                      'units':'',     'hXNbins':3,     'hXmin':0.,     'hXmax':3.,      'cut_pos':230,  'cut_dir':'right'},
    'lep1Pt'              :{'tlatex':'p_{T}^{l1}',                          'units':'GeV',  'hXNbins':50,    'hXmin':0.,     'hXmax':50.,   'cut_pos':230,  'cut_dir':'right'},
    'lep1Eta'             :{'tlatex':'#eta^{l1}',                           'units':'',     'hXNbins':70,    'hXmin':-3.5,   'hXmax':3.5,     'cut_pos':230,  'cut_dir':'right'},
    'lep1Phi'             :{'tlatex':'#phi^{l1}',                           'units':'',     'hXNbins':70,    'hXmin':-3.5,   'hXmax':3.5,     'cut_pos':230,  'cut_dir':'right'},
    'lep2Pt'              :{'tlatex':'p_{T}^{l2}',                          'units':'GeV',  'hXNbins':50,    'hXmin':0.,     'hXmax':50.,    'cut_pos':230,  'cut_dir':'right'},
    'lep2Eta'             :{'tlatex':'#eta^{l2}',                           'units':'',     'hXNbins':1100,  'hXmin':-1000., 'hXmax':100.,    'cut_pos':230,  'cut_dir':'right'},
    'lep2Phi'             :{'tlatex':'#phi^{l2}',                           'units':'GeV',  'hXNbins':1100,  'hXmin':-1000., 'hXmax':100.,    'cut_pos':230,  'cut_dir':'right'},
    'jet1Pt'              :{'tlatex':'p_{T}^{jet1}',                        'units':'GeV',  'hXNbins':50,    'hXmin':0.,     'hXmax':2000.,   'cut_pos':230,  'cut_dir':'right'},
    'jet1Eta'              :{'tlatex':'#eta^{jet1}',                        'units':'',  'hXNbins':70,    'hXmin':-3.5,     'hXmax':3.5,   'cut_pos':230,  'cut_dir':'right'},
    'jet1Phi'              :{'tlatex':'#phi^{jet1}',                        'units':'',  'hXNbins':70,    'hXmin':-3.5,     'hXmax':3.5,   'cut_pos':230,  'cut_dir':'right'},
    'jet2Pt'              :{'tlatex':'p_{T}^{jet2}',                        'units':'GeV',  'hXNbins':50,    'hXmin':0.,     'hXmax':2000.,   'cut_pos':230,  'cut_dir':'right'},      
    'dPhi_lep_met'        :{'tlatex':'#Delta#phi(l_{1}, E_{T}^{miss})',     'units':'',     'hXNbins':16,    'hXmin':0.,     'hXmax':3.2,     'cut_pos':230,  'cut_dir':'right'},
    'DatasetNumber'       :{'tlatex':'DISD',                                'units':'',     'hXNbins':66,    'hXmin':700777, 'hXmax':700843,  'cut_pos':230,  'cut_dir':'right'},
    'fatjet1Pt'           :{'tlatex':'p_{T}^{boosted jet 1}',               'units':'GeV',  'hXNbins':50,    'hXmin':200.,   'hXmax':1000.,   'cut_pos':230,  'cut_dir':'right'},
    'fatjet1Eta'          :{'tlatex':'#eta^{boosted jet 1}',                'units':'',     'hXNbins':70,    'hXmin':-3.5,   'hXmax':3.5,     'cut_pos':230,  'cut_dir':'right'},
    'fatjet1Phi'          :{'tlatex':'#phi^{boosted jet 1}',                'units':'',     'hXNbins':70,    'hXmin':-3.5,   'hXmax':3.5,     'cut_pos':230,  'cut_dir':'right'},
    'fatjet1M'            :{'tlatex':'m^{boosted jet 1}',                   'units':'',     'hXNbins':20,    'hXmin':0,      'hXmax':500,     'cut_pos':230,  'cut_dir':'right'},
    'fatjet1Wtagged'      :{'tlatex':'W tagged',                            'units':'',     'hXNbins':2,     'hXmin':0,      'hXmax':2.,      'cut_pos':230,  'cut_dir':'right'},
    'fatjet1Ztagged'      :{'tlatex':'Z tagged',                            'units':'',     'hXNbins':2,     'hXmin':0,      'hXmax':2.,      'cut_pos':230,  'cut_dir':'right'},
    'fatjet1D2'           :{'tlatex':'D2^{boosted jet 1}',                  'units':'',     'hXNbins':10,    'hXmin':0,      'hXmax':10,      'cut_pos':230,  'cut_dir':'right'},
    'fatjet1Tau21'        :{'tlatex':'#tau^{boosted jet 1}_{21}',           'units':'',     'hXNbins':10,    'hXmin':0,      'hXmax':1.,      'cut_pos':230,  'cut_dir':'right'},
    'fatjet1Tau32'        :{'tlatex':'#tau^{boosted jet 1}_{32}',           'units':'',     'hXNbins':10,    'hXmin':0,      'hXmax':1.,      'cut_pos':230,  'cut_dir':'right'},
    'fatjet1nTrk'         :{'tlatex':'n^{boosted jet 1}_{track}',           'units':'',     'hXNbins':10,    'hXmin':0,      'hXmax':100,     'cut_pos':230,  'cut_dir':'right'},
    'nFatjets'            :{'tlatex':'n^{boosted jet}',                     'units':'',     'hXNbins':10,    'hXmin':0,      'hXmax':10,      'cut_pos':230,  'cut_dir':'right'},
    'fatjet2Pt'           :{'tlatex':'p_{T}^{boosted jet 2}',               'units':'GeV',  'hXNbins':50,    'hXmin':200.,   'hXmax':1000.,   'cut_pos':230,  'cut_dir':'right'},
    'fatjet2M'            :{'tlatex':'m^{boosted jet 2}',                   'units':'',     'hXNbins':20,    'hXmin':0,      'hXmax':500,     'cut_pos':230,  'cut_dir':'right'},
   'trigMatch_singleElectronTrig':{'tlatex':'singleElectronTrig',           'units':'',     'hXNbins':2,     'hXmin':0,      'hXmax':2,       'cut_pos':230,  'cut_dir':'right'},
   'trigMatch_singleMuonTrig'    :{'tlatex':'singleMuonTrig',               'units':'',     'hXNbins':2,     'hXmin':0,      'hXmax':2,       'cut_pos':230,  'cut_dir':'right'},
   'trigMatch_metTrig'    :{'tlatex':'metTrig',                             'units':'',     'hXNbins':2,     'hXmin':0,      'hXmax':2,       'cut_pos':230,  'cut_dir':'right'},
   'bjet1Pt'              :{'tlatex':'p_{T}^{b-jet1}',                        'units':'GeV',  'hXNbins':50,    'hXmin':0.,     'hXmax':2000.,   'cut_pos':230,  'cut_dir':'right'},
   'bjet1Eta'              :{'tlatex':'#eta^{b-jet1}',                        'units':'',  'hXNbins':70,    'hXmin':-3.5,     'hXmax':3.5,   'cut_pos':230,  'cut_dir':'right'},
   'bjet1Phi'              :{'tlatex':'#phi^{b-jet1}',                        'units':'',  'hXNbins':70,    'hXmin':-3.5,     'hXmax':3.5,   'cut_pos':230,  'cut_dir':'right'},
   'amt2'                  :{'tlatex':'am_{T2}',                               'units':'GeV',  'hXNbins':20,    'hXmin':0.,     'hXmax':2000.,   'cut_pos':230,  'cut_dir':'right'},
   'mct'                  :{'tlatex':'m_{CT}',                               'units':'GeV',  'hXNbins':20,    'hXmin':0.,     'hXmax':2000.,   'cut_pos':230,  'cut_dir':'right'},
   'mct2'                  :{'tlatex':'m_{CT2}',                               'units':'GeV',  'hXNbins':20,    'hXmin':0.,     'hXmax':2000.,   'cut_pos':230,  'cut_dir':'right'},
    'mll'                 :{'tlatex':'m_{ll}',                              'units':'GeV',  'hXNbins':20,    'hXmin':50.,    'hXmax':200.,    'cut_pos':230,  'cut_dir':'right'},
    'Rll'                 :{'tlatex':'#DeltaR_{ll}',                              'units':'',  'hXNbins':10,    'hXmin':0.,    'hXmax':10.,    'cut_pos':230,  'cut_dir':'right'},
    'Ptll'                 :{'tlatex':'p_{T}^{ll}',                              'units':'GeV',  'hXNbins':20,    'hXmin':0.,    'hXmax':200.,    'cut_pos':230,  'cut_dir':'right'},
    'dPhill'        :{'tlatex':'#Delta#phi(l_{1}, l_{2})',     'units':'',  'hXNbins':16,    'hXmin':0.,     'hXmax':3.2,     'cut_pos':230,  'cut_dir':'right'},
   'dPhiPllMet'        :{'tlatex':'#Delta#phi(ll, E_{T}^{miss})',     'units':'',  'hXNbins':16,    'hXmin':0.,     'hXmax':3.2,     'cut_pos':230,  'cut_dir':'right'},
   'mjj'                 :{'tlatex':'m_{jj}',                              'units':'GeV',  'hXNbins':30,    'hXmin':0.,    'hXmax':600.,    'cut_pos':230,  'cut_dir':'right'},
   'Rjj'                 :{'tlatex':'#DeltaR_{jj}',                              'units':'',  'hXNbins':10,    'hXmin':0.,    'hXmax':10.,    'cut_pos':230,  'cut_dir':'right'},
    'VAE_loss'            :{'tlatex':'VAE Loss',                            'units':'',     'hXNbins':50,    'hXmin':0.,     'hXmax':100000,  'cut_pos':230,  'cut_dir':'right'},
    'VAE_met'             :{'tlatex':'VAE E_{T}^{miss}',                    'units':'GeV',  'hXNbins':20,    'hXmin':200.,   'hXmax':1000.,   'cut_pos':230,  'cut_dir':'right'},
    'VAE_mt'              :{'tlatex':'VAE m_{T}',                           'units':'GeV',  'hXNbins':20,    'hXmin':0.,     'hXmax':2000.,   'cut_pos':230,  'cut_dir':'right'},
    'VAE_lep1Pt'          :{'tlatex':'VAE p_{T}^{l1}',                      'units':'GeV',  'hXNbins':50,    'hXmin':0.,     'hXmax':2000.,   'cut_pos':230,  'cut_dir':'right'},
    'VAE_jet1Pt'          :{'tlatex':'VAE p_{T}^{jet1}',                    'units':'GeV',  'hXNbins':50,    'hXmin':0.,     'hXmax':2000.,   'cut_pos':230,  'cut_dir':'right'},
    'VAE_jet2Pt'          :{'tlatex':'VAE p_{T}^{jet2}',                    'units':'GeV',  'hXNbins':50,    'hXmin':0.,     'hXmax':2000.,   'cut_pos':230,  'cut_dir':'right'},
    'VAE_dPhi_lep_met'    :{'tlatex':'VAE #Delta#phi(l_{1}, E_{T}^{miss})', 'units':'GeV',  'hXNbins':16,    'hXmin':0.,     'hXmax':3.2,     'cut_pos':230,  'cut_dir':'right'},
  }
  

  return d_vars

